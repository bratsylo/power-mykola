const mobileMenu = document.getElementById('mobile-menu');
const navList = document.querySelector('.nav-list');

const toggle = document.getElementById('themeToggle');
const html = document.querySelector('html');

const themeToggle = document.getElementById('themeToggle');

// Обработчик клика по ссылкам
document.querySelectorAll('.nav-list li a').forEach(link => {
    link.addEventListener('click', function(event) {
        // Предотвращаем переход по ссылке по умолчанию
        event.preventDefault();

        // Получаем ссылку на которую кликнули
        const href = this.getAttribute('href');

        // Получаем текущее состояние переключателя и сохраняем в localStorage
        localStorage.setItem('theme', themeToggle.checked ? 'dark' : 'light');

        // Переходим на другую страницу
        window.location.href = href;
    });
});

toggle.addEventListener('change', function() {
    if (this.checked) {
        html.setAttribute('data-theme', 'dark');
    } else {
        html.setAttribute('data-theme', 'light');
    }
});

// Optional: Check the user's preferred theme from localStorage or system preferences
const currentTheme = localStorage.getItem('theme');
if (currentTheme) {
    html.setAttribute('data-theme', currentTheme);
    if (currentTheme === 'dark') {
        toggle.checked = true;
    }
}

mobileMenu.addEventListener('click', () => {
    const isMenuOpen = navList.style.display === 'flex';
    navList.style.display = isMenuOpen ? 'none' : 'flex';
    mobileMenu.classList.toggle('active');
});
window.addEventListener('resize', () => {
    if (window.innerWidth > 768) {
        navList.style.display = 'flex';
        mobileMenu.classList.remove('active');
    } else if (!mobileMenu.classList.contains('active')) {
        navList.style.display = 'none';
    }
});

function toggleCollapsible(header) {
    const container = header.parentElement;
    container.classList.toggle('active');
    const arrow = header.querySelector('.arrow');
    arrow.innerHTML = container.classList.contains('active') ? '&#9650;' : '&#9660;';
}